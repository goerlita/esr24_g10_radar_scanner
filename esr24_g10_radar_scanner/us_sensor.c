/*
 * us_sensor.c
 *
 *  Created on: 23.06.2020
 *      Author: steddins
 *  Modified on: 28.06.2020
 *      Author_ steddins
 *      Changes:
 *      - Timer2 running in Up-Mode instead of Continuous Mode
 *      - 10 Hz sampling rate
 *      - implementing LPM0
 */

#include <limits.h>
#include "driverlib.h"
#include "Board.h"
#include "us_sensor.h"

uint16_t uiDistance_mm;

void sr04_init() {
    //Einstellen von Timer2_B3: wird f�r die Ansteuerung des US-Sensors verwendet.
    //Gleichzeitig erfolgt �ber den Timer die Messung der Laufzeit im Capture Mode
    //-----------------------------------------------------------------------------
    //Einstellen der Wiederholperiode der US-Messung: up mode
    //Up Mode mit 1 MHz Input --> f�r 100ms Periode: CCR0 = 50000 -1
    Timer_B_clearTimerInterrupt(TB2_BASE);
    Timer_B_initUpModeParam w_param = {0};
    w_param.clockSource = TIMER_B_CLOCKSOURCE_SMCLK;
    w_param.clockSourceDivider = TIMER_B_CLOCKSOURCE_DIVIDER_2;
    w_param.timerPeriod = 50000 - 1;
    w_param.timerInterruptEnable_TBIE = TIMER_B_TBIE_INTERRUPT_DISABLE;
    w_param.captureCompareInterruptEnable_CCR0_CCIE = TIMER_B_CCIE_CCR0_INTERRUPT_DISABLE;
    w_param.timerClear = TIMER_B_DO_CLEAR;
    w_param.startTimer = false;             // der Timer soll mit sr04_start() gestartet werden
    Timer_B_initUpMode(TB2_BASE, &w_param);

    //Einstellung des Capture-Kanals mit dem das Echo-Signal ausgemessen wird: Capture Mode auf CCR1
    //Timer2_B3 CCI1A liegt auf TB2.1 / P5.0 / Pin26;
    //P5.0 as input: ECHO Signal des us-sensors
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P5, GPIO_PIN0, GPIO_PRIMARY_MODULE_FUNCTION);
    //GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P1, GPIO_PIN6, GPIO_SECONDARY_MODULE_FUNCTION);
    Timer_B_initCaptureModeParam cm_param = {0};
    cm_param.captureInputSelect = TIMER_B_CAPTURE_INPUTSELECT_CCIxA;    //P5.0 / TB2.1 CCI1A
    cm_param.captureInterruptEnable = TIMER_B_CAPTURECOMPARE_INTERRUPT_ENABLE;
    cm_param.captureMode = TIMER_B_CAPTUREMODE_RISING_AND_FALLING_EDGE;
    cm_param.captureOutputMode = TIMER_B_OUTPUTMODE_OUTBITVALUE; // nicht erforderlich
    cm_param.captureRegister = TIMER_B_CAPTURECOMPARE_REGISTER_1;
    cm_param.synchronizeCaptureSource = TIMER_B_CAPTURE_SYNCHRONOUS;
    Timer_B_initCaptureMode(TB2_BASE, &cm_param);

    //Einstellung des Compare-Kanals �ber den der Triggerpulse f�r den US-Sensor generiert wird: Compare Mode auf CCR2
    //Da der Counter im Continuous Mode betrieben wird, erfolgt ein �berlauf bei 2^16-1; daraus ergibt sich die Wiederholfrequenz
    //der Messung. Wenn der Counter im Up Mode betrieben w�rde, dann m�ssste CCR0 noch konfiguriert werden.
    //Timer2_B3 CCR2 Ausgang liegt auf P5.1 --> TRIGGER
    //Timer2_B3 CCI1A Eingang f�r Zeitmessung liegt auf Pin P5.0 <---- ECHO
    //Die Erzeugung des Triggerpulses erfolgt �ber den Output PWM Mode; es ist kein Interrupt erforderlich.
    //Die Dauer des Pulses soll lt. Datenblatt 10 �s betragen; gew�hlt: 20�s
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P5, GPIO_PIN1, GPIO_PRIMARY_MODULE_FUNCTION);
    //GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P1, GPIO_PIN7, GPIO_SECONDARY_MODULE_FUNCTION);
    Timer_B_initCompareModeParam initCompParam = {0};
    initCompParam.compareRegister = TIMER_B_CAPTURECOMPARE_REGISTER_2;
    initCompParam.compareInterruptEnable = TIMER_B_CAPTURECOMPARE_INTERRUPT_DISABLE;
    initCompParam.compareOutputMode = TIMER_B_OUTPUTMODE_SET_RESET;
    initCompParam.compareValue = 50000 - 1 - 10;
    Timer_B_initCompareMode(TB2_BASE, &initCompParam);
}

void sr04_start() {
    uiDistance_mm = 0;
    Timer_B_startCounter(TB2_BASE, TIMER_B_UP_MODE);
}

void sr04_stop() {
    Timer_B_stop(TB2_BASE);
    Timer_B_clear(TB2_BASE);
}

uint16_t sr04_get_value() {
    return(uiDistance_mm);
}


#pragma vector=TIMER2_B1_VECTOR
__interrupt void TIMER2_B1_ISR(void)
{
    static uint16_t start = 0;
    static uint16_t state = 0xFFFF;

    switch (__even_in_range(TB2IV, 14))         // Efficient switch-implementation
    {
        case TBIV__TBCCR1:                      // TA1CCR1 : Messung der Dauer des ECHO-Signals
            if ((TB2CCTL1 & CCI) == CCI)
            {   //High-Pegel am CCI Eingang
                //rising edge
                start = TB2CCR1;
                state = 1;
                GPIO_setOutputHighOnPin(GPIO_PORT_LED1, GPIO_PIN_LED1);
            }
            else {                              // Low-Pegel am CCR Eingang
                //falling edge after preceeding rising edge:
                if (state) {
                    uint16_t diff;
                    uint16_t stop;
                    uint32_t length;
                    stop = TB2CCR1;
                    if (stop >= start) {
                        diff = stop - start + 1;
                    }
                    else {
                        diff = stop + (UINT16_MAX - start) + 1; //�berlauf des Z�hlers kompensieren
                    }
                    //Definition des g�ltigen Messbereichs:
                    // Max.: 38 ms / 2�s = 19000
                    // Min.: 2 cm * 2 / (343 m/s) / 2�s = 58
                    if ((diff > 60) && (diff < 20000)) {
                        length = diff * 343L;
                        uiDistance_mm = length / 1000; //Abstand in mm
                    }
                    else
                        uiDistance_mm = 0;
                    state = 0;
                    GPIO_setOutputLowOnPin(GPIO_PORT_LED1, GPIO_PIN_LED1);
                    LPM0_EXIT;
                }
            }
            break;

        case TBIV__TBCCR2:
            // >>>>>>>> Fill-in user code here <<<<<<<<
            break;

        default:
            _no_operation();
            break;
    }
}






