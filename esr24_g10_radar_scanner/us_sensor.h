/*
 * us_sensor.h
 *
 *  Created on: 23.06.2020
 *      Author: steddins
 */

#ifndef US_SENSOR_H_
#define US_SENSOR_H_

#include <stdint.h>

void sr04_init();
void sr04_start();
void sr04_stop();
uint16_t sr04_get_value();


#endif /* US_SENSOR_H_ */
