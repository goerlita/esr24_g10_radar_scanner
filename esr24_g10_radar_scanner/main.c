/**
 * @file main.c
 * @brief Dies ist eine Beschreibung der main.c Datei.
 *
 * Diese Datei enth�lt die Hauptfunktion und die Initialisierungsroutinen f�r das System.
 * Das System verwendet einen MSP430FR2355-Mikrocontroller, um verschiedene Hardware-Komponenten
 * zu steuern, einschlie�lich eines Servos SG90, eines Ultraschallsensor HC-SR04 und eines LC-Displays HD44780.
 *
 * @authors
 * - Alexander G�rlitz
 * - Aaron Kaipf
 * - Christopher Seitz
 * @version 1.0.0
 * @date 04.07.2024
 */

/* --COPYRIGHT--,BSD
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--
 */

/*
 * Version 1.0.0
 *
 * Bestimmungsgem��er Gebrauch:
 * 1. Launchpad �ber USB-Buchse mit Spannung versorgen
 *
 * Ultraschallsensor Verdrahtung:
 *
 *                 MSP430FR2355                                 5V---|
 *             -----------------                                 ----|-------
 *         /|\|                 |                               |
 *          | |    P5.1/TB2.2out|---> US-TRIG (Pin2)----------->|US-TRIG (Pin2)
 *          --|RST              |                               |
 *            |         P1.0    |---> red LED           --------|US-ECHO (Pin3)
 *            |                 |                       |       |
 *            |                 |                      1,5k     |SR04
 *            |   P5.0/TB2.1CCIA|<----------------------|        ----|-------
 *            |                 |                      2,2k          |
 *            |                 |                       |           GND
 *                                                     GND
 *
 * LCD und I2C Verdrahtung:
 *                MSP430FR2355          PCF8574T
 *                    master
 *              -----------------
 *            -|XIN  P1.2/UCB0SDA|<-------->| SDA
 *             |                 |          |
 *            -|XOUT             |          |
 *             |     P1.3/UCB0SCL|<-------->|SCL
 *    LEDred<--|P1.0             |          |
 *                                   5V-->| Vcc
 *
 *  MSP430FR235x Demo - Timer1_B3, PWM TB1.1-2, Up Mode, DCO SMCLK
 *
 *
 * Servo Verdrahtung:
 *           MSP430FR2355
 *         ---------------
 *     /|\|               |
 *      | |               |
 *      --|RST            |
 *        |               |
 *        |     P2.0/TB1.1|--> CCR1 - XX% PWM
 *        |               |
 *
 *   Darren Lu
 *   Texas Instruments Inc.
 *   Oct. 2016
 *   Built with IAR Embedded Workbench v6.50 & Code Composer Studio v6.2
 */

#include "driverlib.h"
#include "Board.h"
#include "us_sensor.h"
#include "lcd1602.h"
#include <stdio.h>

//Target frequency for MCLK in kHz
#define CS_MCLK_DESIRED_FREQUENCY_IN_KHZ   1000
//MCLK/FLLRef Ratio
#define CS_MCLK_FLLREF_RATIO   30

// Der PCF8574 Baustein hat folgende 7-Bit Adresse: 0x3F (dez 63) : 0011 1111
// Somit ergibt sich f�r Schreibzugriffe folgender Adresswert: 0111 1110 = 0x7E
// Die Driverlib erwartet jedoch die Angabe der tats�chlichen 7-Bit Adresse, also 0x3F
/**
 * @brief I2C-Slave-Adresse f�r den PCF8574 I/O Expander.
 *
 * Diese Definition legt die I2C-Slave-Adresse f�r den PCF8574 I/O Expander fest.
 * Je nach Version des PCF8574 (PCF8574T oder PCF8574AT) wird die entsprechende Adresse gew�hlt.
 * - PCF8574T: Adresse 0x27
 * - PCF8574AT: Adresse 0x3F
 */
#define SLAVE_ADDRESS 0x27

///initialisiert die pins
void init_gpio(void);
void init_cs(void);

///initialisiert den Timer
void init_timer(void);
///initialisiert I2C
void init_i2c(void);
///Sleep f�r bestimmte anzahl clock cycles
void sleep(uint16_t ms);


void detectedSomething(int pPosition, uint16_t pDistance);
void configurePWM(int pwmSignal);
void startUp();

/**
 * @brief Hauptfunktion f�r die Interaktivit�t.
 *
 * Diese Funktion initialisiert die Hardware, konfiguriert den PWM f�r den Servo,
 * startet den Ultraschallsensor und verarbeitet die Entfernungsmessungen, um
 * erkannte Patienten anzuzeigen.
 */
void main (void)
{
    uint16_t distance;
    char string1[17] = "";      //null-Terminierung belegt 17. Zeichen
    char string2[17] = "";      //wichtig: den String auf ganzer L�nge definieren,
                                //damit am an den letzten Stellen nicht noch der
                                //Rest von der letzten Ausgabe auftaucht.

    WDT_A_hold(WDT_A_BASE);     //Stop watchdog timer
    // Servo-Initialisierung
    P2DIR |= BIT0;              // P2.0 output
    P2SEL0 |= BIT0;             // P2.0 options select
    // Disable the GPIO power-on default high-impedance mode to activate
    // previously configured port settings

    // Gr�ne LED-Initialisierung
    P6DIR |= 0x40;              // Set P6.6 to output direction
    // Gr�ne LED-Initialisierung Ende

    PM5CTL0 &= ~LOCKLPM5;       // Port-Einstellungen freigeben

    TB1CCR0 = 20000-1;          // PWM Period
    TB1CCTL1 = OUTMOD_7;        // CCR1 reset/set
    // Servo-Initialisierung Ende

    init_gpio();                 // GPIO initialisieren

    //LCD
    init_timer();
    init_cs();
    init_i2c();
    //LCD End

    sr04_init();                // HC-SR04-Ultraschallsensor initialisieren
    __bis_SR_register(GIE);     // Globalen Interrupt aktivieren
    sr04_start();               // HC-SR04-Ultraschallsenso starten

    lcd1602_init();             // LCD initialisieren
    lcd1602_backlight(true);    // LCD-Hintergrundbeleuchtung einschalten

    lcd1602_write(1,string1);
    lcd1602_write(2,string2);

    int abstandsErkennung = 1500;   // Der Abstand in mm, innerhalb dessen das System etwas erkennen soll
    int startGrad = 1000;           // Der Wert 500 entspricht -45�
    int endGrad = 2000;             // Der Wert 2500 entspricht 135�
    int pwmSteps = 66;              // 90�/16 Display-Ziffern = 5,625� pro Ziffer
                                    // Bereich = 1000 PWM; 1000 PWM/90� = 62,5 PWM/� = pwmSteps
    int pwmValue = 0;               // Der PWM-Wert, der an den Servo gesendet wird
    int i = 8;                      // Z�hler f�r die Schleife, startet bei 8, damit der Servo in der Mitte ist (45�)
    int detectionCounter = 0;       // Z�hler, um das Display zur�ckzusetzen


    startUp();                  // Initialisierungsroutine ausf�hren

    while (1)
    {
        // Aufw�rts z�hlen von 1000 bis 2000
        for (i = i; startGrad + pwmSteps*i <= endGrad; i++) {
            pwmValue = startGrad + pwmSteps * i;
            configurePWM(pwmValue);             // Setzt PWM auf den aktuellen Wert
            distance = sr04_get_value();
            if(distance<=abstandsErkennung){
                detectedSomething(i, distance);
                detectionCounter = 0;
            }
            __delay_cycles(250000);             // Sleep 0,25 sekunden (bei 1 MHz clock)
        }
        detectionCounter++;

        // Abw�rts z�hlen von 2000 bis 1000
        for (i = i-1; startGrad + pwmSteps*i >= startGrad;i--) {
            pwmValue = startGrad + pwmSteps * i;
            configurePWM(pwmValue);             // Setzt PWM auf den aktuellen Wert
            distance = sr04_get_value();
            if(distance<=abstandsErkennung){
                detectedSomething(i, distance);
                detectionCounter = 0;
            }
            __delay_cycles(250000);             // Sleep 0,25 sekunden (bei 1 MHz clock)
        }

        detectionCounter++;
        if(detectionCounter >= 2){
            lcd1602_clear();
        }

        i = 0;

        __no_operation();
    }
}

/**
 * @brief Konfiguriert das PWM-Signal.
 *
 * Diese Funktion konfiguriert das PWM-Signal mit einem angegebenen Wert. Der PWM-Signalwert
 * kann offiziell zwischen 1000 und 2000 liegen, jedoch sind auch Werte zwischen 500 und 2500
 * m�glich, was einem Bereich von 180 Grad entspricht.
 *
 * @param pwmSignal Der PWM-Signalwert, zwischen 500 und 2500.
 */
void configurePWM(int pwmSignal){
    // Setzen des PWM duty cycle f�r CCR1
    TB1CCR1 = pwmSignal;
    // Konfiguration des Timers
    TB1CTL = TBSSEL__SMCLK | MC__UP | TBCLR;
}


/**
 * @brief Startet die Initialisierungsprozedur beim Hochfahren des MSP430.
 *
 * Diese Funktion f�hrt eine Prozedur aus, die beim Hochfahren des MSP430 aufgerufen wird.
 * Sie schaltet eine gr�ne LED ein und f�hrt eine Reihe von PWM-Konfigurationen und Verz�gerungen durch,
 * bevor sie die LED wieder ausschaltet.
 */
void startUp(){
    // Toggle P6.6 using exclusive-OR, es leuchtet die gr�ne LED
    P6OUT ^= 0x40;
    // Konfiguriere PWM mit einem Wert von 1500 und warte
    configurePWM(1500);
    __delay_cycles(1000000);
    // Konfiguriere PWM mit einem Wert von 500 und warte
    configurePWM(500);
    __delay_cycles(1000000);
    // Konfiguriere PWM mit einem Wert von 2500 und warte
    configurePWM(2500);
    __delay_cycles(1000000);
    // Konfiguriere PWM mit einem Wert von 1500 und warte
    configurePWM(1500);
    __delay_cycles(1000000);
    // Toggle P6.6 using exclusive-OR, gr�ne LED wird ausgeschaltet
    P6OUT ^= 0x40;
}

/**
 * @brief Diese Methode wird ausgef�hrt, wenn der HC-SR04-Ultraschallsensor etwas erkennt.
 *
 * Diese Funktion wird aufgerufen, wenn der SR04-Sensor einen Patienten erkennt. Sie schaltet eine
 * gr�ne LED ein, zeigt die erkannte Entfernung und die Position auf einem LCD-Display an und
 * schaltet die LED wieder aus.
 *
 * @param i Die Position des erkannten Objekts.
 * @param pDistance Die gemessene Entfernung zum erkannten Objekt in Millimetern.
 */
void detectedSomething(int pPosition, uint16_t pDistance ){
    // Toggle P6.6 using exclusive-OR, es leuchtet die gr�ne LED
    P6OUT ^= 0x40;

    // Erstellung eines char-Array mit einer L�nge von 17
    char array[17];
    // Initialisierung des Arrays mit Leerzeichen
    int j;
    for (j = 0; j < 17; j++) {
        array[j] = ' ';
    }
    // Einsetzen des Buchstaben 'x' an die gew�nschte Position
    if (pPosition >= 0 && pPosition < 17) { // �berpr�fen, ob die Position im g�ltigen Bereich liegt
        if(pPosition == 16){
            array[16-pPosition] = 'x';
        }else{
            array[16-pPosition-1] = 'x';
        }
    }
    char string17[17];

    // Formatierung der Entfernung als Zeichenkette und Anzeige auf dem LCD
    if(pDistance == 0){
        pDistance = 1500;
    }
    sprintf(string17, "%dmm    ", pDistance); // z. B. Ausgabe: 1499mm
    lcd1602_write(1,string17);
    lcd1602_write(2,array);
    // Toggle P6.6 using exclusive-OR, gr�ne LED wird ausgeschaltet
    P6OUT ^= 0x40;
}


void init_gpio(void) {
    //Set LED1 to output direction
    GPIO_setAsOutputPin(
        GPIO_PORT_LED1,
        GPIO_PIN_LED1
        );
    PMM_unlockLPM5();
}

//LCD
void init_timer(void) {
    static Timer_B_initUpModeParam param = {0};

    param.clockSource = TIMER_B_CLOCKSOURCE_SMCLK;
    param.clockSourceDivider = TIMER_B_CLOCKSOURCE_DIVIDER_1;
    param.timerPeriod = 999;        // wenn 1000 Taktimpulse gez�hlt
                                    // wurden, erfolgt ein Interrupt
                                    // Periodendauer somit 1ms
    param.timerInterruptEnable_TBIE =
            TIMER_B_TBIE_INTERRUPT_DISABLE;         // no interrupt on 0x0000
    param.captureCompareInterruptEnable_CCR0_CCIE =
        TIMER_B_CAPTURECOMPARE_INTERRUPT_ENABLE;    // interrupt on TRmax
    param.timerClear = TIMER_B_DO_CLEAR;
    param.startTimer = true;

    // start Timer:
    Timer_B_initUpMode(TB0_BASE, &param);
}

void init_cs(void) {
    //Set DCO FLL reference = REFO
    CS_initClockSignal(
        CS_FLLREF,
        CS_REFOCLK_SELECT,
        CS_CLOCK_DIVIDER_1
        );

    //Set ACLK = REFO
    CS_initClockSignal(
        CS_ACLK,
        CS_REFOCLK_SELECT,
        CS_CLOCK_DIVIDER_1
        );

    //Create struct variable to store proper software trim values
    CS_initFLLParam param = {0};

    //Set Ratio/Desired MCLK Frequency, initialize DCO, save trim values
    CS_initFLLCalculateTrim(
        CS_MCLK_DESIRED_FREQUENCY_IN_KHZ,
        CS_MCLK_FLLREF_RATIO,
        &param
        );

    //Clear all OSC fault flag
    CS_clearAllOscFlagsWithTimeout(1000);

    //Enable oscillator fault interrupt
    SFR_enableInterrupt(SFR_OSCILLATOR_FAULT_INTERRUPT);
}

//LCD
void init_i2c(void) {
    EUSCI_B_I2C_initMasterParam param = {0};

    // Configure Pins for I2C
    /*
    * Select Port 1
    * Set Pin 2, 3 to input with function, (UCB0SIMO/UCB0SDA, UCB0SOMI/UCB0SCL).
    */

    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_UCB0SCL, GPIO_PIN_UCB0SCL, GPIO_FUNCTION_UCB0SCL);
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_UCB0SDA, GPIO_PIN_UCB0SDA, GPIO_FUNCTION_UCB0SDA);

    param.selectClockSource = EUSCI_B_I2C_CLOCKSOURCE_SMCLK;
    param.i2cClk = CS_getSMCLK();
    param.dataRate = EUSCI_B_I2C_SET_DATA_RATE_100KBPS;
    param.byteCounterThreshold = 1;
    param.autoSTOPGeneration = EUSCI_B_I2C_NO_AUTO_STOP;
    EUSCI_B_I2C_initMaster(EUSCI_B0_BASE, &param);

    //Specify slave address
    EUSCI_B_I2C_setSlaveAddress(EUSCI_B0_BASE, SLAVE_ADDRESS);
    //Set in transmit mode
    EUSCI_B_I2C_setMode(EUSCI_B0_BASE, EUSCI_B_I2C_TRANSMIT_MODE);
    //Enable I2C Module to start operations
    EUSCI_B_I2C_enable(EUSCI_B0_BASE);
}

//LCD
// Die Funktion kehrt erst dann zur�ck, wenn Timer0_B3 die angegebene Anzahl von
// ms absolviert hat. Unbedingt beachten: Diese einfache Implementierung eines
// sleep-Timers funktioniert nur, solange kein Interrupt nesting verwendet wird.
// W�hrend der sleep-Perioden des Timer k�nnen andere Interrupt-Routinen ausgef�hrt
// werden; diese d�rfen aber nicht in die main loop zur�ckkehren, sondern m�ssen
// den aktuellen sleep mode beim Verlassen der ISR wieder herstellen.
void sleep(uint16_t ms) {
    while (ms--) LPM0;
}

#pragma vector=UNMI_VECTOR
__interrupt void NMI_ISR(void)
{
    uint16_t status;
    do {
        // If it still can't clear the oscillator fault flags after the timeout,
        // trap and wait here.
        status = CS_clearAllOscFlagsWithTimeout(1000);
    } while(status != 0);
}

//LCD
// TimerB0 Interrupt Vector (TBxIV) handler
#pragma vector=TIMER0_B0_VECTOR
__interrupt void TIMER0_B0_ISR(void)
{
    __bic_SR_register_on_exit(LPM0_bits);
}
